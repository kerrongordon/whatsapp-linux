[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://bitbucket.org/kerrongordon/whatsapp-linux)

# WhatsApp Linux

[![Greenkeeper badge](https://badges.greenkeeper.io/kerrongordon/whatsapp-linux.svg)](https://greenkeeper.io/)
[![Build Status](https://travis-ci.org/kerrongordon/whatsapp-linux.svg?branch=master)](https://travis-ci.org/kerrongordon/whatsapp-linux)

Simple. Secure. Reliable messaging. With WhatsApp, you'll get fast, simple, secure messaging and calling for free*, available on phones all over the world.

## Download For Linux

[Linux Packages](https://github.com/kerrongordon/whatsapp-linux/releases/latest)

## How To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) and [Yarn](https://yarnpkg.com)

```bash
# Clone this repository
git clone https://github.com/kerrongordon/whatsapp-linux.git
# Go into the repository
cd whatsapp-linux
# Install dependencies
yarn install
# Run the app
yarn start
# Build the app
yarn prod
```

